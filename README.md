## The ItsMyCargo Guides

#### [Ruby](../ruby-general/)
#### [CSS & Sass](../css-sass/)
#### [JavaScript: CSS](../javascript-css/)
#### [JavaScript: General](../javascript-general/)
#### [JavaScript: React](../javascript-react/)
___

## Version control
*To ensure all got the same versions heres a version-guideline*
### [node: v8.10.0 - *includes npm v5.6.0*](https://nodejs.org/en/download/current/)
### [postgreSQL: v9.5.12](https://www.postgresql.org/ftp/source/v9.5.12/)
### Ruby: 17-12-14 revision 61247) [x86_64-linux]
### MongoDB: shell version v3.6.1
### git version 2.7.4

___
## License
The style guides are modified versions of the popular Airbnb style guides.
All Code Style Guides in this repository are subject to this [**License**](LICENSE.md).