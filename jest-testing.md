# Testing Framework
The environment is built with [jest](https://facebook.github.io/jest/) (with its [expect](https://facebook.github.io/jest/docs/en/expect.html) flavor, not to be confused with [mjackson's flavour](https://github.com/mjackson/expect)) and [enzyme](http://airbnb.io/enzyme/).

## Bootstrap
Jest starts each test file in a separate process. Each process runs the `/jest.init.js` as its first file and then runs the requested test file afterwards. Global test specific polyfills are to be configured in that file. Right now it is used initialize Enzyme with the React 16 Adapter and it adds a new validator (`toBeType`) to expect.

## Test files
A test file is a javascript file with a specific format: ***.test.js** or ***.test.jsx**. To keep the association to the file tested, it should be named after the file its being written for, and placed in the same folder.

Example: A file named `Loading.jsx` in `app/components/Loading` should have its tests saved in `app/components/Loading/Loading.test.jsx`

## Globals
Jest defines a few globals for each test file.
- `jest`: access to the global [jest object](https://facebook.github.io/jest/docs/en/jest-object.html)
- `expect`: access to the expect function
- The different test specific [globals](https://facebook.github.io/jest/docs/en/api.html)

### Test Globals
#### [`beforeAll(fn, timeout)`](https://facebook.github.io/jest/docs/en/api.html#beforeallfn-timeout) and [`afterAll(fn, timeout)`](https://facebook.github.io/jest/docs/en/api.html#afterallfn-timeout)
The specified function is run *before* and *after* the requested test file is run. `beforeAll` is run before the requested file is run, if you put the code in the global state, the code is run immediatly. If the runner is executing multiple files, this might create race conditions, prefer `beforeAll`.

#### [`beforeEach(fn, timeout)`](https://facebook.github.io/jest/docs/en/api.html#beforeeachfn-timeout) and  [`afterEach(fn, timeout)`](https://facebook.github.io/jest/docs/en/api.html#aftereachfn-timeout)
Those functions are run before and after a test in the current and descending scopes are run.

#### [`describe[.only/.skip](name, fn)`](https://facebook.github.io/jest/docs/en/api.html#describename-fn)
Describes a new block / scope. The name is displayed in the test runner output. The suffix `.skip` will skip the whole block. If one or more `.only` calls are found in the current scope, only those will be run.

This scope should only include calls to `test`

####  [`test[.only/.skip](name, fn, timeout)`](https://facebook.github.io/jest/docs/en/api.html#testname-fn-timeout)
Describes an actual test. The `.skip` and `.only` suffixes work similar to the ones for `describe`.

The name is displayed in the console. If you specify a timeout, the test is aborted with an error as soon as the timeout is hit.

The test function should contain code that is needed to prepare and run the code. Its output should be checked using the `expect` global function.

## Test structure
Use `describe` to define the current scope. For a small help, you could replace `describe` in your mind with *When* or *With*. You can nest as many describe blocks as you want.

In a describe block, you can nest as many `test` calls as you see fit / need for your test. To help give names to your test, replace `test` in your mind with *It* or *This*. Try to not use "should". (Prefer "It does this" over "It should do this").

Negative tests are as important as positive tests. Test for expected negative outcomes as well as for expected results.

### React testing
You can import `shallow`, `render` and `mount` from `enzyme` for easier React testing. Try to avoid using `render` though. It is slow, first it renders the whole react component chain, then converts it to a virtual dom using jsdom, and then uses [Cheerio](https://github.com/cheeriojs/cheerio) to access the results.

If possible use `shallow` and `mount`. Shallow only renders the top most level of the component. It does not render calls to nested components, they get ignored and removed from the DOM. If you need to test for nested components (or check if they exist/get rendered), use `mount` instead.

## Example test
```jsx
import React from 'react'
import { shallow, mount } from 'enzyme'
// eslint-disable-next-line import/no-named-as-default
import Alert from './Alert'

describe('components/Alert', () => {
  test('exports a valid react element', () => {
    const wrapper = shallow(<Alert onClose={() => {}} message={{ type: 'success', text: 'lorem ipsum' }} />)
    expect(wrapper.exists()).toBeTruthy()
  })

  describe('sets the correct message type class', () => {
    [
      ['error', 'danger'],
      ['alert', 'warning'],
      ['notice', 'info'],
      ['success', 'success']
    ].forEach(([type, className]) => {
      test(`${type} maps to ${className}`, () => {
        const wrapper = shallow(<Alert onClose={() => {}} message={{ type, text: 'lorem ipsum' }} />).render()
        const node = wrapper.find('.alert')
        expect(node).toHaveLength(1)
        expect(node.first().hasClass(className)).toBeTruthy()
      })
    })

    test(`unkown type falls back to success`, () => {
      const wrapper = mount(<Alert onClose={() => {}} message={{ type: '', text: 'lorem ipsum' }} />)
      const node = wrapper.find('.alert')
      expect(node).toHaveLength(1)
      expect(node.first().hasClass('success')).toBeTruthy()
    })
  })

  test('runs the onClick handler on close', () => {
    const onClick = jest.fn()
    const wrapper = mount(<Alert onClose={onClick} message={{ type: 'success', text: 'lorem ipsum' }} />)
    wrapper
      .find('.close')
      .first()
      .simulate('click')
    expect(onClick).toHaveBeenCalledTimes(1)
  })

  test('runs the onClick handler on timeout', () => {
    const onClick = jest.fn()
    jest.useFakeTimers()
    mount(<Alert onClose={onClick} message={{ type: 'success', text: 'lorem ipsum' }} />)
    jest.runAllTimers()
    expect(setTimeout).toHaveBeenCalledTimes(1)
    expect(onClick).toHaveBeenCalledTimes(1)
    jest.useRealTimers()
  })
})
```
