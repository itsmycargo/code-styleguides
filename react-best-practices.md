# React Best Practices

## Keep it as simple as possible
If you write components, make sure to keep it as simple as possible.

If you don't need to access or modify any state, prefer to use _Pure Functions_ over components. Pure Functions are basically functions that receive the `props` as the first parameter. It should not do heavy adjustments to the props and everything in the function gets called each time the props are adjusted.

If you need to access state, prefer to use _Pure Components_ over real components. Pure Components are like real components, but ignores the provided `shouldComponentUpdate` methods. For more details, see the [official documentation](https://reactjs.org/docs/react-api.html#reactpurecomponent).

## Component Types
Decide if your component is concerned with presentation or logic. If you have a presentational component, it does not need the hows and whys, it just presents stuff. A container (or logic) component, just gathers data and restructures it for the presentation logic. It is basically a simple MVVM (Model - View - ViewModel) architecture. Read more on [Medium](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0).

## Code style
```js
// export the default component (1)
export default class MyComponent extends React.Component {  
	// use es7 property definition (2)
	state = { expanded: false }

	// add a proptype definition (3)
	static propTypes = {  
		model: PropTypes.instanceOf(MyModel).isRequired,  
		title: PropTypes.string  
	}
	
	// add default proptype definition (3)
	static defaultProps = {  
		title: 'Your Name'  
	}
	
	render() {
		// destructure the props / state (4)
		const {
			model,
			title
		} = this.props
	}
}
```

1. Always export your component as the default export. If you decide to include higher order components, export the chained component as default, and export the unchained one via a named export. Don't define more then one component per file.

2. If possible, use ES7 property definition for components. Property definitions are new to ES7 and allow a much cleaner initialization.

3. Always add a proptype definition and define defaults for at least the optional props. Try to avoid the `array`, `object`, and `any` types. While the first two provide a rough idea what to expect, the last one is very unclear. Try to use `arrayOf()` instead of `array`, so you know what type of array you expect. Instead of `object`, you could use `instanceOf()` (when you expect instances), `objectOf()` (if the object is used as a hashmap) or `shape()` (if you need to manually define the layout). For more details see [the documentation](https://reactjs.org/docs/typechecking-with-proptypes.html).

4. Prefer to destructor the props (and state) in the early lines of the render method. This way, the code looks a bit cleaner, and you immediatly see what props and state is needed for rendering.

## Handlers
Handlers are an important aspect of programming. Always prefix props that are expecting handlers with `on`, this way you know that a function/handler is expected to appear. This will also inline the codestyle with the HTML style of defining events. 

For handler implementations, agree on a common naming scheme. Usual prefixes are `on` or `handle`. Try to avoid calls to `.bind(this)` - instead use arrow functions in the JSX.

```jsx
<MyComponent onSomething={(param, ev) => this.onSomething(param, ev)} />
```

## File Hierachy
Don't try to split components. A presentational and container component are on the same level. Higher Order Components and non components can be abstracted into a different file tree with different naming schemes.

The (internal) component name should be similar to the path. For example a `UserProfileEdit` component, should be found in `user/profile/edit/index.jsx` or `user/profile/edit.jsx`. The path should be all lowercase to prevent errors using different environments.

This will allow a nice categorization by file path / classes.

```
user
|-- profile
|   |-- edit
|   |   `-- index.jsx
|   `-- popout.jsx
|-- index.jsx
`-- settings
    `-- edit.jsx
```
